/* jshint node: true, strict: global, esversion: 6 */
'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('attendance', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        user_id: {
            type: DataTypes.INTEGER,
            references: {
                model: 'users',
                key: 'id'
            },
            allowNull: false
        },
        conference_id: {
            type: DataTypes.INTEGER,
            references: {
                model: 'conferences',
                key: 'id'
            },
            allowNull: false
        },
        enter_date_time: {
            type: DataTypes.DATE,
            allowNull: false
        },
        exit_date_time: {
            type: DataTypes.DATE,
            allowNull: false
        }
    },{
        underscored: true
    });
};
