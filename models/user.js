/* jshint node: true, strict: global, esversion: 6 */
'use strict';

let _ = require('underscore');

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('user', {
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                isEmail: true
            }
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                len: [1, 255]
            }
        },
        last_name: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                len: [1, 255]
            }
        },
        photo_url: {
            type: DataTypes.STRING,
            allowNull: false
        },
        epc: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
        }
    },{
        underscored: true,
        instanceMethods: {
            toPublicJSON: function() {
                let json = this.toJSON();
                let pickedJson = _.pick(json, 'epc', 'email', 'name', 'last_name', 'photo_url');
                return pickedJson;
            }
        }
    });
};
