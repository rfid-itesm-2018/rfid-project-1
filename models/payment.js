/* jshint node: true, strict: global, esversion: 6 */
'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('payment', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        user_id: {
            type: DataTypes.INTEGER,
            references: {
                model: 'users',
                key: 'id'
            },
            allowNull: false
        },
        conference_id: {
            type: DataTypes.INTEGER,
            references: {
                model: 'conferences',
                key: 'id'
            },
            allowNull: false
        },
        card_termination: {
            type: DataTypes.STRING,
            allowNull: false
        },
        amount: {
            type: DataTypes.DOUBLE,
            allowNull: false,
            validate: {
                isGreaterThanZero: function(value) {
                    if (typeof value !== 'number' || value < 0.0) {
                        throw new Error('Price must be positive');
                    }
                }
            }
        }
    },{
        underscored: true
    });
};
