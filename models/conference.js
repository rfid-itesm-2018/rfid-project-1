/* jshint node: true, strict: global, esversion: 6 */
'use strict';

let _ = require('underscore');

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('conference', {
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                len: [1, 255]
            }
        },
        start_date_time: {
            type: DataTypes.DATE,
            allowNull: false
        },
        end_date_time: {
            type: DataTypes.DATE,
            allowNull: false
        },
        price: {
            type: DataTypes.DOUBLE,
            allowNull: false,
            validate: {
                isGreaterThanZero: function(value) {
                    if (typeof value !== 'number' || value < 0.0) {
                        throw new Error('Price must be positive');
                    }
                }
            }
        }
    },{
        underscored: true,
        instanceMethods: {
            toPublicJSON: function() {
                let json = this.toJSON();
                return _.pick(json, 'id','name', 'start_date_time', 'end_date_time', 'price');
            }
        }
    });
};
