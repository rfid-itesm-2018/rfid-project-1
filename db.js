/* jshint node: true, strict: global, esversion: 6 */
'use strict';

let Sequelize = require('sequelize');
let env = process.env.NODE_ENV || 'development';
let sequelize;
if (env === 'production') {
    sequelize = new Sequelize(process.env.DATABASE_URL, {
        dialect: 'postgres'
    });
} else {
    sequelize = new Sequelize(undefined, undefined, undefined, {
        'dialect': 'sqlite',
        'storage': __dirname + '/data/dev-todo-api.sqlite'
    });
}

let db = {};

db.user = sequelize.import(__dirname + '/models/user.js');
db.conference = sequelize.import(__dirname + '/models/conference.js');
db.payment = sequelize.import(__dirname + '/models/payment.js');
db.attendance = sequelize.import(__dirname + '/models/attendance.js');
db.sequelize = sequelize;
db.Sequelize = Sequelize;

db.conference.belongsToMany(db.user, {
    as: 'ConferenceUsersPayments',
    through: {
        model: db.payment,
        unique: false
    },
    onDelete: 'CASCADE'
});
db.user.belongsToMany(db.conference, {
    as: 'UserConferencesPayments',
    through: {
        model: db.payment,
        unique: false
    },
    onDelete: 'CASCADE'
});

db.conference.belongsToMany(db.user, {
    as: 'ConferenceUsersAttendance',
    through: {
        model: db.attendance,
        unique: false
    },
    onDelete: 'CASCADE'
});
db.user.belongsToMany(db.conference, {
    as: 'UserConferencesAttendance',
    through: {
        model: db.attendance,
        unique: false
    },
    onDelete: 'CASCADE'
});

module.exports = db;
