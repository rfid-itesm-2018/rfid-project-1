/* jshint node: true, strict: global, esversion: 6 */
'use strict';

let express = require('express');
let app = express();
let bodyParser = require('body-parser');
let db = require('./db.js');
let _ = require('underscore');


let PORT = process.env.PORT || 8080;
let server;

let env = process.env.NODE_ENV || 'development';

// Process every request with this middleware and parse it to JSON
app.use(bodyParser.json());

app.use(function(error, req, res, next) {
    if (error instanceof SyntaxError) {
        res.status(400).json({
            errors: [{
                "text": "Sintax error"
            }]
        });
    } else {
        next();
    }
});

// Routes
require('./controllers/users_controllers')(app, db);
require('./controllers/conferences_controller')(app, db);


// Handle 404 - Keep this as a last route
app.use(function(req, res, next) {
    res.status(404).send('Cannot ' + req.method + ' ' + req.url + '\n');
});
app.use(function (err, req, res, next) {
    console.error(err.stack);
    res.status(500).send();
});


db.sequelize.sync({
    //force: true
}).then(function() {
    server = app.listen(PORT, function() {
        console.log('Express server started on port ' + PORT + '!');
    });
    server.timeout = 20000;
}).catch(function (error) {
    console.log('Error starting server');
    console.log(error);
});
