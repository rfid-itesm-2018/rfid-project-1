/* jshint node: true, strict: global, esversion: 6 */
'use strict';

let _ = require('underscore');

module.exports = function (app, db) {
    app.post('/users', function(req, res) {
        let body = _.pick(req.body, 'epc', 'name', 'last_name', 'email', 'photo_url');

        db.user.findOne({
            where: {
                epc: body.epc
            }
        }).then(function (user) {
            if(user) {
                res.status(409).json({
                    message: 'Este EPC ya está registrado'
                });
            } else {
                db.user.create(body).then(function (user) {
                    res.json({
                        message: 'El usuario fue creado correctamente'
                    });
                }).catch(function () {
                    res.status(500).json({
                        message: 'Error del servidor'
                    });
                });
            }
        }).catch(function () {
            res.status(500).json({
                message: 'Error del servidor'
            });
        });
    });

    app.get('/users/:epc', function (req, res) {
        let epc = req.params.epc;

        db.user.findOne({
            where: {
                epc: epc
            }
        }).then(function (user) {
            if(user) {
                res.json(user.toPublicJSON());
            } else {
                res.status(404).json({
                    message: 'Este EPC no ha sido registrado'
                });
            }
        }).catch(function (err) {
            res.status(500).json({
                message: 'Error del servidor'
            });
        });
    });

    app.put('/users/:epc', function (req, res) {
        let epc = req.params.epc;
        let body = _.pick(req.body, 'name', 'last_name', 'email', 'photo_url');

        db.user.findOne({
            where: {
                epc: epc
            }
        }).then(function (user) {
            if(user) {
                user.update(body).then(function (updatedUser) {
                    res.json({
                        message: 'El usuario fue modificado correctamente'
                    });
                }).catch(function () {
                    res.status(500).json({
                        message: 'Error del servidor'
                    });
                });
            } else {
                res.status(404).json({
                    message: 'Este EPC no ha sido registrado'
                });
            }
        }).catch(function (err) {
            res.status(500).json({
                message: 'Error del servidor'
            });
        });
    });

    app.get('/users/:epc/conferences', function (req, res) {
        let epc = req.params.epc;

        db.user.findOne({
            where: {
                epc: epc
            }
        }).then(function (user) {
            if(user) {
                let query = 'SELECT c.id, c.name, c.start_date_time, c.end_date_time, c.price FROM conferences  AS c JOIN payments AS p ON c.id=p.conference_id WHERE p.user_id=:user_id;';
                db.sequelize.query(query,{
                    replacements:{
                        user_id: user.get('id')
                    },
                    type: db.sequelize.QueryTypes.SELECT
                }).then(function (conferences) {
                    res.json({
                        conferences: conferences
                    });
                }).catch(function () {
                    res.status(500).json({
                        message: 'Error del servidor'
                    });
                });
            } else {
                res.status(404).json({
                    message: 'Este EPC no ha sido registrado'
                });
            }
        }).catch(function (err) {
            res.status(500).json({
                message: 'Error del servidor'
            });
        });
    });

    app.get('/users', function (req, res) {
        db.user.findAll({}).then(function (users) {
            res.json({
                users: users.map((u) => u.toPublicJSON())
            });
        }).catch(function () {
            res.status(500).json({
                message: 'Error del servidor'
            });
        });
    });
};
