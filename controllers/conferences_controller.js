/* jshint node: true, strict: global, esversion: 6 */
'use strict';

let _ = require('underscore');
let moment = require('moment');

module.exports = function(app, db){
    app.post('/conferences', function (req, res) {
        let body = _.pick(req.body, 'name', 'start_date_time', 'end_date_time', 'price');
        db.conference.create({
            name: body.name,
            start_date_time: new Date(body.start_date_time),
            end_date_time: new Date(body.end_date_time),
            price: body.price
        }).then(function (conference) {
            res.json({
                message: 'La conferencia fue creada correctamente'
            });
        }).catch(function (err) {
            console.log(err);
            res.status(500).json({
                message: 'Error del servidor'
            });
        });
    });

    app.get('/conferences', function (req, res) {

        db.conference.findAll({

        }).then(function (conferences) {
            res.json({
                conferences: conferences.map((conference) => conference.toPublicJSON())
            });
        }).catch(function (err) {
            console.log(err);
            res.status(500).json({
                message: 'Error del servidor'
            });
        });
    });

    app.put('/conferences/:id', function (req, res) {
        let id = req.params.id;
        let body = _.pick(req.body, 'name', 'start_date_time', 'end_date_time', 'price');

        db.conference.findOne({
            where: {
                id: id
            }
        }).then(function (conference) {
            if(conference) {
                conference.update(body).then(function (updatedConference) {
                    res.json({
                        message: 'La conferencia fue modificada correctamente'
                    });
                }).catch(function () {
                    res.status(500).json({
                        message: 'Error del servidor'
                    });
                });
            } else {
                res.status(404).json({
                    message: 'Esta conferencia no existe'
                });
            }
        }).catch(function (err) {
            res.status(500).json({
                message: 'Error del servidor'
            });
        });
    });

    app.get('/conferences/:id', function (req, res) {
        let id = req.params.id;
        db.conference.findOne({
            where: {
                id: id
            }
        }).then(function (conference) {
            if(conference) {
                res.json(conference.toPublicJSON());
            } else {
                res.status(404).json({
                    message: 'Esta conferencia no existe'
                });
            }
        }).catch(function (err) {
            res.status(500).json({
                message: 'Error del servidor'
            });
        });
    });

    app.post('/pay/:conference_id', function (req, res) {
        let conference_id = req.params.conference_id;
        let body = _.pick(req.body, 'tarjeta', 'epc');

        db.conference.findOne({
            where: {
                id: conference_id
            }
        }).then(function (conference) {
            if(conference) {
                db.user.findOne({
                    where: {
                        epc: body.epc
                    }
                }).then(function (user) {
                    if(user) {
                        db.payment.create({
                            user_id: user.get('id'),
                            conference_id: conference.get('id'),
                            card_termination: body.tarjeta.substr(body.tarjeta.length - 4),
                            amount: conference.get('price')
                        }).then(function () {
                            res.json({
                                message: 'Pago exitoso'
                            });
                        }).catch(function (err) {
                            console.log(err);
                            res.status(500).json({
                                message: 'Error del servidor'
                            });
                        });
                    } else {
                        res.status(404).json({
                            message: 'El EPC no ha sido registrado'
                        });
                    }
                });
            } else {
                res.status(404).json({
                    message: 'La conferencia no existe'
                });
            }
        }).catch(function (err) {
            res.status(500).json({
                message: 'Error del servidor'
            });
        });
    });

    app.get('/payments', function (req, res) {
        db.payment.findAll({}).then(function (payments) {
            res.json({
                payments: payments
            });
        }).catch(function () {
            res.status(500).json({
                message: 'Error del servidor'
            });
        });
    });

    app.post('/conferences/:conference_id/enter/:epc', function (req, res) {
        let conference_id = req.params.conference_id;
        let epc = req.params.epc;

        db.conference.findOne({
            where: {
                id: conference_id
            }
        }).then(function (conference) {
            if (conference) {
                db.user.findOne({
                    where: {
                        epc: epc
                    }
                }).then(function (user) {
                    if(user) {
                        let startDateTime = new Date(conference.get('start_date_time'));
                        let endDateTime = new Date(conference.get('end_date_time'));
                        let currentDate = new Date();

                        if(startDateTime <= currentDate && endDateTime >= currentDate){
                            db.payment.findOne({
                                where: {
                                    user_id: user.get('id'),
                                    conference_id: conference.get('id')
                                }
                            }).then(function (payment) {
                                if(payment){
                                    db.attendance.create({
                                        user_id: user.get('id'),
                                        conference_id: conference.get('id'),
                                        enter_date_time: new Date(),
                                        exit_date_time: new Date()
                                    }).then(function () {
                                        res.json({
                                            message: 'El usuario si puede entrar: ' + user.get('name') + ' ' + user.get('last_name')
                                        });
                                    }).catch(function () {
                                        res.status(500).json({
                                            message: 'Error del servidor'
                                        });
                                    });
                                } else {
                                    res.status(403).json({
                                        message: 'El usuario no ha pagado para entrar a la conferencia'
                                    });
                                }
                            }).catch(function () {
                                res.status(500).json({
                                    message: 'Error del servidor'
                                });
                            });
                        } else {
                            if(startDateTime > currentDate){
                                res.status(403).json({
                                    message: 'La conferencia todavía no ha empezado'
                                });
                            } else {
                                res.status(403).json({
                                    message: 'La conferencia ya terminó'
                                });
                            }
                        }
                    } else {
                        res.status(404).json({
                            message: 'Este EPC no ha sido registrado'
                        });
                    }
                }).catch(function () {
                    res.status(500).json({
                        message: 'Error del servidor'
                    });
                });
            } else {
                res.status(404).json({
                    message: 'Esta conferencia no existe'
                });
            }
        }).catch(function () {
            res.status(500).json({
                message: 'Error del servidor'
            });
        });
    });

    app.get('/conferences/:id/stats', function (req, res) {
        let id = req.params.id;
        db.conference.findOne({
            where: {
                id: id
            }
        }).then(function (conference) {
            if(conference) {
                let queryPayedUsers = 'SELECT COUNT(DISTINCT(a.user_id)) AS stats FROM (SELECT user_id FROM attendances WHERE conference_id=:conference_id) AS a UNION ALL SELECT COUNT(DISTINCT(p.user_id)) AS STATS FROM (SELECT user_id FROM payments WHERE conference_id=:conference_id) AS p;';
                db.sequelize.query(queryPayedUsers,{
                    replacements:{
                        conference_id: conference.get('id')
                    },
                    type: db.sequelize.QueryTypes.SELECT
                }).then(function (result) {
                    let attendance = result[0].stats;
                    let payments = result[1].stats;
                    res.json({
                        conference: conference.toPublicJSON(),
                        stats: {
                            payed_users: payments,
                            attendance: attendance
                        }
                    });
                }).catch(function () {
                    res.status(500).json({
                        message: 'Error del servidor'
                    });
                });
            } else {
                res.status(404).json({
                    message: 'Esta conferencia no existe'
                });
            }
        }).catch(function () {
            res.status(500).json({
                message: 'Error del servidor'
            });
        });
    });

    app.get('/stats', function(req, res){
        let query = 'SELECT c.id AS id, c.name AS name, c.start_date_time AS start_date_time, c.end_date_time AS end_date_time, attendance_table.attendance AS attendance FROM conferences AS c JOIN (SELECT COUNT (conference_id) attendance, conference_id FROM (SELECT DISTINCT user_id, conference_id from attendances) AS a GROUP BY conference_id ORDER BY COUNT (conference_id) DESC) as attendance_table ON c.id=attendance_table.conference_id;';
        db.sequelize.query(query,{
            type: db.sequelize.QueryTypes.SELECT
        }).then(function (results) {
            let popular = results.slice(0, 5).map(function(element){
                return {
                    conference: {
                        id: element.id,
                        name: element.name,
                        start_date_time: element.start_date_time,
                        end_date_time: element. end_date_time,
                        price: element.price
                    },
                    attendance: element.attendance
                };
            });

            res.json({ popular });
        }).catch(function () {
            res.status(500).json({
                message: 'Error del servidor'
            });
        });
    });
};