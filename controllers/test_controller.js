/* jshint node: true, strict: global, esversion: 6 */
'use strict';

module.exports = function (app, db) {
    app.get('/test', function(req, res) {
        res.json({
            message: 'From /test'
        });
    });
};
